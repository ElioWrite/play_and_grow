﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanelVM : PanelViewModel
{
    [SerializeField]
    private Text _scoresText;

    public void Initialize()
    {
        _scoresText.text = Root.Data.User.ScoresFromUserData.ToString();
    }

    public void OnQuestBtnClicked()
    {
        Root.States.GoToState(StateCode.QuestGenerating);
    }

    public void OnStatsBtnClicked()
    {

    }

    public void OnExitBtnClicked()
    {

    }
}
