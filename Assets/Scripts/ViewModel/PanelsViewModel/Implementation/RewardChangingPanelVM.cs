﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardChangingPanelVM : PanelViewModel
{
    public void OnBetChangedBtnClicked(int bet)
    {
        if(bet <= Root.Data.User.ScoresFromUserData)
        {
            Root.User.SetRandomQuest(bet);
            Root.States.GoToState(StateCode.QuestDisplaying);
        }
    }
}
