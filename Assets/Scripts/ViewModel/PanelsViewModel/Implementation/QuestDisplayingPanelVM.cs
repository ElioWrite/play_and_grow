﻿using UnityEngine;
using UnityEngine.UI;

public class QuestDisplayingPanelVM : PanelViewModel
{
    [SerializeField]
    private Image _iconImage;

    [SerializeField]
    private Text _actionHeader;

    [SerializeField]
    private Text _count;

    [SerializeField]
    private Text _actionDescription;

    public void Initialize(QuestModel quest)
    {
        var desc = quest.Data.Description;

        var strings = desc.Split(new string[] { "{0}" }, System.StringSplitOptions.RemoveEmptyEntries);

        if (strings.Length > 0)
        {
            if (strings.Length >= 1)
                _actionHeader.text = strings[0].ToUpper().Replace(" ", "");
            if(strings.Length >= 2)
                _actionDescription.text = strings[1].ToUpper().Replace(" ", "");
        }

        _count.text = quest.Amount.ToString();

        _iconImage.sprite = quest.Data.Icon;
    }

    public void OnDoneBtnClicked()
    {
        Root.User.OnUserCompleteQuest(true);
        Root.States.GoToState(StateCode.QuestRewarding);
    }

    public void OnLoseBtnClicked()
    {
        Root.User.OnUserCompleteQuest(false);
        Root.States.GoToState(StateCode.QuestRewarding);
    }
}
