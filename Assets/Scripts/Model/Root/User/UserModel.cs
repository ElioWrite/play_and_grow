﻿using System;
using UnityEngine;

public class UserModel : Model
{
    [SerializeField]
    private int _startPoints;
    public int StartPoints => _startPoints;

    public QuestModel CurrentQuest { get; private set; }

    public UserStatus Status { get; private set; }

    public void RefreshCurrentQuest()
    {
        if (Root.Data.User.IsCurrentQuestExist)
        {
            CurrentQuest = Root.Quests.GetQuestFromJson(Root.Data.User.UserData.CurrentQuest);
            Status = UserStatus.Challenging;
        }
        else throw new InvalidOperationException("Current Quest Data isn't exist");
    }

    public void SetRandomQuest(int bet)
    {
        CurrentQuest = Root.Quests.GetRandomQuest(bet);
    }

    public void OnUserCompleteQuest(bool isWin)
    {
        Status = isWin ? UserStatus.Winning : UserStatus.Losing;
    }
}

public enum UserStatus
{
    None = 0,
    Challenging = 10,
    Winning = 20,
    Losing = 30
}
