﻿using UnityEngine;

public class QuestsModel : Model
{
    /// <summary>
    /// Генерация случайного квеста на основе случайно выбранной даты;
    /// </summary>
    public QuestModel GetRandomQuest(int reward)
    {
        var data = Root.Data.Quests.RandomQuestData;

        return new QuestModel(data, reward, Random.Range(data.MinAmount, data.MaxAmount + 1));
    }

    public QuestModel GetQuestFromJson(JsonQuestData data) => new QuestModel(data.QuestID,
            data.Reward,
            data.Amount,
            Root.Data.Quests.GetQuestDataByType(data.Type));
}
