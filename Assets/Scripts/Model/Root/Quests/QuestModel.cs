﻿using System;

public class QuestModel
{
    public Guid QuestID { get; private set; }
    public int Reward { get; private set; }
    public int Amount { get; private set; }
    public QuestDataModel Data { get; private set; }

    public DateTime GenerationDate { get; private set; }

    public DateTime? CompletionDate { get; private set; }

    public JsonQuestData ToJsonData =>
        new JsonQuestData(QuestID, Data.Type, QuestResult.None, Reward, Amount, GenerationDate, CompletionDate ?? DateTime.Now);

    public QuestModel(QuestDataModel dataModel, int reward, int amount)
    {
        if (dataModel == null)
            throw new ArgumentNullException("dataModel");

        Reward = reward;

        Data = dataModel;

        Amount = amount;

        QuestID = Guid.NewGuid();

        GenerationDate = DateTime.Now;
    }

    public QuestModel(Guid id, int reward, int amount, QuestDataModel data)
    {
        Reward = reward;
        Amount = amount;
        Data = data;

        QuestID = id;

        GenerationDate = DateTime.Now;
    }
}

public enum QuestType
{
    None = 0,
    Reading = 10,
    SitUps = 20,
    PushUps = 30,
}
