﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardChangingStateModel : StateModel
{
    public override IEnumerator OnStateBeginning()
    {
        yield return base.OnStateBeginning();

        yield return UI.Panels.ShowPanel<RewardChangingPanelVM>(TransitionCode.Alpha);
    }

    public override IEnumerator OnStateEnding()
    {
        yield return base.OnStateEnding();

        Root.Data.User.AddCurrentQuest(Root.User.CurrentQuest.ToJsonData);

        yield return UI.Panels.HidePanel<RewardChangingPanelVM>(TransitionCode.Alpha);
    }
}
