﻿using System.Collections;
using UnityEngine;

public class QuestGeneratingStateModel : StateModel
{
    public override IEnumerator OnStateBeginning()
    {
        yield return base.OnStateBeginning();

        yield return UI.Panels.ShowPanel<QuestGeneratingPanelVM>(TransitionCode.Alpha);

        yield return new WaitForSeconds(1);

        if(Root.Data.User.IsCurrentQuestExist)
        {
            Root.User.RefreshCurrentQuest();
            yield return Root.States.GoToStateCoroutine(StateCode.QuestDisplaying);
        }
        else yield return Root.States.GoToStateCoroutine(StateCode.RewardChanging);
    }

    public override IEnumerator OnStateEnding()
    {
        yield return base.OnStateEnding();

        yield return UI.Panels.HidePanel<QuestGeneratingPanelVM>(TransitionCode.Alpha);
    }
}
