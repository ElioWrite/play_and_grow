﻿using System.Collections;
using UnityEngine;

public class QuestDisplayingStateModel : StateModel
{
    public override IEnumerator OnStateBeginning()
    {
        yield return base.OnStateBeginning();

        UI.Panels.ViewModel.GetPanel<QuestDisplayingPanelVM>().Initialize(Root.User.CurrentQuest);

        yield return UI.Panels.ShowPanel<QuestDisplayingPanelVM>(TransitionCode.Alpha);
    }

    public override IEnumerator OnStateEnding()
    {
        yield return base.OnStateEnding();

        yield return UI.Panels.HidePanel<QuestDisplayingPanelVM>(TransitionCode.Alpha);
    }
}
