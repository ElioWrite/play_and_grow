﻿using System.Collections;
using UnityEngine;

public class MainMenuStateModel : StateModel
{
    public override IEnumerator OnStateBeginning()
    {
        yield return base.OnStateBeginning();

        UI.Panels.ViewModel.GetPanel<MainMenuPanelVM>().Initialize();

        yield return UI.Panels.ShowPanel<MainMenuPanelVM>(TransitionCode.Alpha);

        Debug.Log(Root.Data.User.UserData);
    }

    public override IEnumerator OnStateEnding()
    {
        yield return base.OnStateEnding();

        yield return UI.Panels.HidePanel<MainMenuPanelVM>(TransitionCode.Alpha);
    }
}
