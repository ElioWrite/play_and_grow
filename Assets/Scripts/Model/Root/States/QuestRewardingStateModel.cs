﻿using System.Collections;
using UnityEngine;

public class QuestRewardingStateModel : StateModel
{
    public override IEnumerator OnStateBeginning()
    {
        yield return base.OnStateBeginning();

        yield return UI.Panels.ShowPanel<QuestRewardingPanelVM>(TransitionCode.Alpha);

        if(Root.User.Status == UserStatus.Winning)
            Root.Data.User.ApplyCurrentQuest(QuestResult.Win);
        else Root.Data.User.ApplyCurrentQuest(QuestResult.Lose);

        yield return new WaitForSeconds(1);

        yield return Root.States.GoToStateCoroutine(StateCode.MainMenu);
    }

    public override IEnumerator OnStateEnding()
    {
        yield return base.OnStateEnding();

        yield return UI.Panels.HidePanel<QuestRewardingPanelVM>(TransitionCode.Alpha);
    }
}
