﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QuestsDataModel : Model
{
    [SerializeField]
    private QuestDataModel[] _quests;
    public QuestDataModel[] Quests => _quests;

    public QuestDataModel RandomQuestData => _quests[Random.Range(0, _quests.Length)];

    public QuestDataModel GetQuestDataByType(QuestType type) => _quests.FirstOrDefault(t => t.Type == type);
}
