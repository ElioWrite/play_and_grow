﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestDataModel : Model
{
    [SerializeField]
    private QuestType _type;
    public QuestType Type => _type;

    [SerializeField]
    private string _description;
    public string Description => _description;

    [SerializeField]
    private int _minAmount;
    public int MinAmount => _minAmount;

    [SerializeField]
    private int _maxAmount;
    public int MaxAmount => _maxAmount;

    [SerializeField]
    private Sprite _icon;
    public Sprite Icon => _icon;
}
