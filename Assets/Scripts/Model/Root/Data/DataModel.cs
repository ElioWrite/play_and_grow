﻿using UnityEngine;

public class DataModel : Model
{
    [SerializeField]
    private UserDataModel _userDataModel;
    public UserDataModel User => _userDataModel;

    [SerializeField]
    private QuestsDataModel _questsDataModel;
    public QuestsDataModel Quests => _questsDataModel;
}
