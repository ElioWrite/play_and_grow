﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

public class UserDataModel : Model
{
    public string DataPath => Path.Combine(Application.persistentDataPath, "Saved Files", "UserData.json");

    public const string PrefsDataKey = "User Data";

    public JsonUserData UserData { get; private set; }

    public bool IsCurrentQuestExist => UserData.CurrentQuest.QuestID != Guid.Empty;

    public bool IsUserDataExists => PlayerPrefs.HasKey(PrefsDataKey);

    public void FetchUserData()
    {
        if (IsUserDataExists)
            UserData = JsonUtility.FromJson<JsonUserData>(PlayerPrefs.GetString(PrefsDataKey));
        else ClearUserData();
    }

    public void AddCompletedQuests(JsonQuestData completedQuest)
    {
        var quests = UserData.CompletedQuests.ToList();

        quests.Add(completedQuest);

        UserData = new JsonUserData(UserData.AdditionalPoints, UserData.CurrentQuest, quests.ToArray());

        PlayerPrefs.SetString(PrefsDataKey, UserData.ToString());
    }

    public void AddCurrentQuest(JsonQuestData questData)
    {
        UserData = new JsonUserData(UserData.AdditionalPoints, questData, UserData.CompletedQuests);

        PlayerPrefs.SetString(PrefsDataKey, UserData.ToString());
    }

    public void ClearCurrentQuest()
    {
        UserData = new JsonUserData(UserData.AdditionalPoints, new JsonQuestData(), UserData.CompletedQuests);

        PlayerPrefs.SetString(PrefsDataKey, UserData.ToString());
    }

    public void ApplyCurrentQuest(QuestResult result)
    {
        UserData.CurrentQuest = Root.User.CurrentQuest.ToJsonData;
        UserData.CurrentQuest.Result = result;

        AddCompletedQuests(UserData.CurrentQuest);
        ClearCurrentQuest();
    }

    public void ClearUserData()
    {
        UserData = new JsonUserData(Root.User.StartPoints, new JsonQuestData(), new JsonQuestData[] { });

        PlayerPrefs.SetString(PrefsDataKey, UserData.ToString());
    }

    public void AddAdditionalPoints(int points)
    {
        UserData = new JsonUserData(UserData.AdditionalPoints + points, 
            UserData.CurrentQuest, 
            UserData.CompletedQuests);

        PlayerPrefs.SetString(PrefsDataKey, UserData.ToString());
    }

    public int ScoresFromUserData
    { 
        get 
        {
            var scores = UserData.AdditionalPoints;
            foreach (var q in UserData.CompletedQuests)
            {
                if (q.Result == QuestResult.Win)
                    scores += q.Reward;
                else if (q.Result == QuestResult.Lose)
                    scores -= q.Reward;
            }
            return scores;
        } 
    }
}

[System.Serializable]
public class JsonUserData
{
    public int AdditionalPoints;
    public JsonQuestData CurrentQuest;
    public JsonQuestData[] CompletedQuests;

    public JsonUserData(int addPoints, JsonQuestData currentQuest, JsonQuestData[] completedQuests)
    {
        AdditionalPoints = addPoints;
        CurrentQuest = currentQuest;
        CompletedQuests = completedQuests;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }
}

[System.Serializable]
public class JsonQuestData
{
    public Guid QuestID;
    public QuestType Type;
    public QuestResult Result;
    public int Reward;
    public int Amount;
    public DateTime GenerationDate;
    public DateTime CompletionDate;

    public JsonQuestData(Guid questID, QuestType type, QuestResult result, int reward, int amount, DateTime generationDate, DateTime completionDate)
    {
        QuestID = questID;
        Type = type;
        Reward = reward;
        Amount = amount;
        GenerationDate = generationDate;
        CompletionDate = completionDate;
    }

    public JsonQuestData()
    {
        QuestID = Guid.Empty;
        Type = QuestType.None;
        Reward = 0;
        Amount = 0;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }
}

public enum QuestResult
{
    None = 0,
    Win = 10,
    Lose = 20
}
